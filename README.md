**Reddit Code Challenge**

From Agustin Tomas Larghi.

I've attached a video, a screenshot and an apk build there on the repo. Sorry, I did the code challenge in one sitting during the weekend, I didn't have that much time to do the code challenge during the week, that's why I coded the solution in one sitting. Hopefully the comments that I've dropped on the code are enough so you guys can follow my trail of thought.


About the architecture, this is an standard multi-module MVVM architecture, I'm relying on Room to provide the info that I show on the list. The solution was heavily inspired on the architectures that I presented here on these articles:

https://dev.to/4gus71n/a-production-level-architecture-for-android-apps-part-1-271m

Hope you guys like it! (and even if you not, I do really appreciate all feedback! So please let me know if I can improve the architecture here somehow)
Thanks!