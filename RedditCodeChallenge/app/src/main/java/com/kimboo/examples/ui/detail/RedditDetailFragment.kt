package com.kimboo.examples.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kimboo.core.models.RedditPost
import com.kimboo.core.utils.MyViewModelFactory
import com.kimboo.core.utils.getBaseSubComponent
import com.kimboo.core.utils.glide.GlideApp
import com.kimboo.core.utils.glide.loadRedditThumbnail
import com.kimboo.examples.R
import com.kimboo.examples.di.component.DaggerExampleViewInjector
import com.kimboo.examples.di.component.ExampleViewInjector
import com.kimboo.examples.ui.detail.viewmodel.RedditDetailViewModel
import com.kimboo.examples.ui.list.viewmodel.RedditListViewModel
import kotlinx.android.synthetic.main.fragment_reddit_detail.*
import javax.inject.Inject

class RedditDetailFragment : Fragment() {

    // TODO
    private lateinit var redditPostId: String

    // region Variables declaration
    @Inject
    lateinit var viewModelProvider: MyViewModelFactory
    lateinit var viewModel: RedditDetailViewModel

    private val viewInjector: ExampleViewInjector
        get() = DaggerExampleViewInjector.builder()
            .baseSubComponent(requireActivity().getBaseSubComponent())
            .build()
    // endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewInjector.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelProvider)
            .get(RedditDetailViewModel::class.java)

        redditPostId = arguments?.getString(ARG_BUNDLE_REDDIT_POST_ID) ?: throw RuntimeException("You need to pass the ARG_BUNDLE_REDDIT_POST_ID")
    }

    private fun observeStateChanges() {
        viewModel.state.observe(this, Observer {
            when (it) {
                is RedditDetailViewModel.State.Success -> {
                    onLoadRedditPost(it.redditPost)
                }
                is RedditDetailViewModel.State.Error -> {
                    onShowErrorState()
                }
            }
        })
    }

    private fun onShowErrorState() {
        // NOOP
    }

    private fun onLoadRedditPost(redditPost: RedditPost) {
        fragmentRedditDetailTitleTextView.text = redditPost.titleText
        GlideApp.with(fragmentRedditDetailThumbnailImageView)
            .loadRedditThumbnail(redditPost, R.drawable.drawable_thumbnail_placeholder)
            .into(fragmentRedditDetailThumbnailImageView)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_reddit_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeStateChanges()
    }

    override fun onStart() {
        super.onStart()
        viewModel.fetchRedditPostById(redditPostId)
    }

    companion object {
        private const val ARG_BUNDLE_REDDIT_POST_ID = "ARG_BUNDLE_REDDIT_POST_ID"

        fun newInstance(redditPostId: String): Fragment {
            return RedditDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_BUNDLE_REDDIT_POST_ID, redditPostId)
                }
            }
        }
    }
}