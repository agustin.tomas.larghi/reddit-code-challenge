package com.kimboo.examples.ui.list.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kimboo.core.interactors.GetRedditPostsInteractor
import com.kimboo.core.interactors.DismissAllRedditPostInteractor
import com.kimboo.core.interactors.MarkRedditPostAsRemovedInteractor
import com.kimboo.core.models.RedditPost
import javax.inject.Inject

/**
 * The ViewModel that we use on the [RedditListActivity]
 */
class RedditListViewModel @Inject constructor(
    private val getRedditPostsInteractor: GetRedditPostsInteractor,
    private val markRedditPostAsRemovedInteractor: MarkRedditPostAsRemovedInteractor,
    private val dismissAllRedditPostsInteractor: DismissAllRedditPostInteractor
) : ViewModel(), GetRedditPostsInteractor.Callback, MarkRedditPostAsRemovedInteractor.Callback,
    DismissAllRedditPostInteractor.Callback {

    // region Sealed classes declaration
    sealed class State {
        data class Success(
            val list: List<RedditPost>
        ) : State()
        object Error : State()
    }
    // endregion

    // region Variables declaration
    val isLoading = MutableLiveData<Boolean>()
    val state = MutableLiveData<State>()
    // endregion

    // region DismissAllRedditPostInteractor.Callback implementation
    override fun onSuccessfullyRemovedAllRedditPosts() {
        fetchNewPage()
    }

    override fun onErrorTryingToRemoveAllRedditPosts() {
        state.value = State.Error
    }
    // endregion

    // region MarkRedditPostAsRemovedInteractor.Callback implementation
    override fun onSuccessfullyRemovedRedditPost() {
        // TODO We could add functionality here to UNDO the dismissal of the reddit post
    }

    override fun onErrorTryingToRemoveRedditPost() {
        state.value = State.Error
    }
    // endregion

    // region GetRedditPostsInteractor.Callback implementation
    override fun onRedditPostsSuccessfullyFetched(repositories: List<RedditPost>) {
        state.value = State.Success(
            list = repositories
        )
    }

    override fun onErrorFetchingRedditPosts() {
        isLoading.value = false
        state.value = State.Error
    }

    override fun onDoneLoadingFromApi(it: List<RedditPost>?) {
        isLoading.value = false
    }
    // endregion

    fun removeRedditPost(redditPost: RedditPost) {
        markRedditPostAsRemovedInteractor.execute(this, redditPost.id)
    }

    fun fetchRedditPosts() {
        isLoading.value = true
        getRedditPostsInteractor.execute(this, false)
    }

    fun fetchNewPage() {
        isLoading.value = true
        getRedditPostsInteractor.execute(this, true)
    }

    fun removeAllRedditPosts() {
        isLoading.value = true
        dismissAllRedditPostsInteractor.execute(this)
    }
}