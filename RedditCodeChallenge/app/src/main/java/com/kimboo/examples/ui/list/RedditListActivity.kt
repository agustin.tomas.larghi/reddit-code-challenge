package com.kimboo.examples.ui.list

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.kimboo.base.utils.PaginatedScrollListener
import com.kimboo.core.models.RedditPost
import com.kimboo.core.utils.MyViewModelFactory
import com.kimboo.core.utils.getBaseSubComponent
import com.kimboo.examples.R
import com.kimboo.examples.di.component.DaggerExampleViewInjector
import com.kimboo.examples.di.component.ExampleViewInjector
import com.kimboo.examples.ui.detail.RedditDetailActivity
import com.kimboo.examples.ui.detail.RedditDetailFragment
import com.kimboo.examples.ui.list.adapter.RedditAdapter
import com.kimboo.examples.ui.list.adapter.RedditDiffCallback
import com.kimboo.examples.ui.list.viewmodel.RedditListViewModel
import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.include_reddit_list.*
import javax.inject.Inject

/**
 * Displays a list with all the top posts from Reddit.
 */
class RedditListActivity : AppCompatActivity(), RedditAdapter.Callback {

    // region Variables declaration
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false

    /**
     * DI variables related to injecting the ViewModels
     */
    @Inject
    lateinit var viewModelProvider: MyViewModelFactory
    lateinit var viewModel: RedditListViewModel

    private val viewInjector: ExampleViewInjector
        get() = DaggerExampleViewInjector.builder()
            .baseSubComponent(getBaseSubComponent())
            .build()

    /**
     * The reddit adapter.
     */
    private val redditAdapter = RedditAdapter(this, this)
    // endregion

    // region RedditAdapter.Callback implementation

    /**
     * Callback for when we click on a reddit post.
     */
    override fun onRedditPostClicked(redditPost: RedditPost) {
        if (twoPane) {
            val fragment = RedditDetailFragment.newInstance(redditPost.id)
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit()
        } else {
            startActivity(RedditDetailActivity.getStartIntent(this, redditPost.id))
        }
    }

    /**
     * Callback for when we click on the dismiss btn from a reddit post.
     */
    override fun onDismissPostBtnClicked(redditPost: RedditPost) {
        viewModel.removeRedditPost(redditPost)
    }

    // endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        if (findViewById<NestedScrollView>(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        // Injection the ViewModel
        viewInjector.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelProvider)
            .get(RedditListViewModel::class.java)

        // Initialize the views
        setupRecyclerView()
        setupDismissAllView()
        setupSwipeRefreshLayout()

        observeIsLoadingChanges()
        observeStateChanges()

        // Fetch the reddit posts
        viewModel.fetchRedditPosts()
    }

    /**
     * We setup the click of the dismiss all view.
     */
    private fun setupDismissAllView() {
        activityListDismissAllBtn.setOnClickListener {
            viewModel.removeAllRedditPosts()
        }
    }

    /**
     * We observe the state changes from the ViewModel.
     */
    private fun observeStateChanges() {
        viewModel.state.observe(this, Observer {
            when (it) {
                is RedditListViewModel.State.Success -> {
                    onShowRepositoryList(it.list)
                }
                is RedditListViewModel.State.Error -> {
                    onDisplayErrorMessage()
                }
            }
        })
    }

    /**
     * Function triggered when something goes wrong while trying to fetch the reddit posts. No internet connection,
     * or an error response from the backend.
     */
    private fun onDisplayErrorMessage() {
        Snackbar.make(
            mainActivityExamplesSwipeRefreshLayout,
            getString(R.string.main_activity_error_message),
            Snackbar.LENGTH_LONG
        ).show()
    }

    /**
     * Function triggered after we successfully fetch the reddit posts. We update the adapter.
     */
    private fun onShowRepositoryList(newList: List<RedditPost>) {
        val oldList = redditAdapter.redditPosts.toList()

        redditAdapter.redditPosts.apply {
            clear()
            addAll(newList)
        }

        val diffResult = DiffUtil.calculateDiff(RedditDiffCallback(oldList, newList))
        diffResult.dispatchUpdatesTo(redditAdapter)
    }

    /**
     * We observe the loading changes and based on that we show the ProgressBar from the SwipeToRefresh
     * layout and update the state of the pagination scroll listener so we prevent multiple queries to
     * the backend.
     */
    private fun observeIsLoadingChanges() {
        viewModel.isLoading.observe(this, Observer {
            mainActivityExamplesSwipeRefreshLayout.isRefreshing = it
            paginationScrollListener.isLoading = it
        })
    }

    /**
     * Initialize the SwipeRefreshLayout. If we do a Pull To Refresh we fetch the posts again.
     */
    private fun setupSwipeRefreshLayout() {
        mainActivityExamplesSwipeRefreshLayout.setOnRefreshListener {
            viewModel.fetchRedditPosts()
        }
    }

    /**
     * The pagination listener, if it gets triggered we fetch the next available page.
     */
    private val paginationScrollListener = object : PaginatedScrollListener() {
        override fun onFetchNextPage() {
            viewModel.fetchNewPage()
        }
    }

    /**
     * Initialize the RecyclerView, we set the adapter and the pagination scroll listener.
     */
    private fun setupRecyclerView() {
        mainActivityExamplesRecyclerView.adapter = redditAdapter
        mainActivityExamplesRecyclerView.addOnScrollListener(paginationScrollListener)
    }
}