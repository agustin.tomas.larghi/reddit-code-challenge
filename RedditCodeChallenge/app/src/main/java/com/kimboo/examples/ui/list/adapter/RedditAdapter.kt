package com.kimboo.examples.ui.list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kimboo.core.models.RedditPost
import com.kimboo.core.utils.glide.GlideApp
import com.kimboo.core.utils.glide.loadRedditThumbnail
import com.kimboo.examples.R
import kotlinx.android.synthetic.main.view_holder_example_item.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * ViewHolder declaration for the list of reddit posts.
 */
class RedditPostViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val hasBeenReadView: View = view.viewHolderExampleReadView
    val userNameTextView: TextView = view.viewHolderUserNameTextView
    val createdAtTextView: TextView = view.viewHolderCreatedAtTextView
    val thumbnailImageView: ImageView = view.viewHolderExampleThumbnailImageView
    val titleTextView: TextView = view.viewHolderExampleTitleTextView
    val dismissBtn: TextView = view.viewHolderExampleDismissPostTextView
    val commentCountTextView: TextView = view.viewHolderExampleCommentCountTextView
}

/**
 * The adapter that we use to render the list of Reddit posts.
 */
class RedditAdapter(
    // The context to instantiate the inflater.
    context: Context,
    // The callback that we use to communicate the events back to the Activity.
    var callback: Callback
) : RecyclerView.Adapter<RedditPostViewHolder>() {

    // Variables that we use to format the created at timestamp.
    private val calendar = Calendar.getInstance()
    private val timeFormatter = SimpleDateFormat("h:mma")
    private val dayFormatter = SimpleDateFormat("EEEE")
    private val fullDateFormatter = SimpleDateFormat("MMMM d")
    private val fullYearFormatter = SimpleDateFormat("MMMM d, y")

    /**
     * Callback interface that we use to communicate the events back to the activity.
     */
    interface Callback {
        fun onRedditPostClicked(repository: RedditPost)
        fun onDismissPostBtnClicked(redditPost: RedditPost)
    }

    /**
     * The inflater that we use to inflate the ViewHolder's view. I know that some people instantiate this
     * on the fly every time you call the [onCreateViewHolder] via the [parent.context] but I find this approach
     * better in terms of performance, since it is initialized only once, the first time.
     */
    private val inflater: LayoutInflater by lazy {
        LayoutInflater.from(context)
    }

    /**
     * The collection that we use to keep track of the reddit posts.
     */
    val redditPosts = mutableListOf<RedditPost>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RedditPostViewHolder {
        return RedditPostViewHolder(
            inflater.inflate(R.layout.view_holder_example_item, parent, false)
        )
    }

    override fun getItemCount() = redditPosts.size

    override fun onBindViewHolder(holder: RedditPostViewHolder, position: Int) {
        val context = holder.itemView.context
        val redditPost = redditPosts[position]
        // Set the text of the reddit post
        holder.titleTextView.text = redditPost.titleText
        // Set the username
        holder.userNameTextView.text = redditPost.userName
        // Set the visibility of the reading badge
        holder.hasBeenReadView.visibility = if (!redditPost.hasBeenRead) {
            View.VISIBLE
        } else {
            View.GONE
        }
        // Set the amount of comments
        holder.commentCountTextView.text = context.resources.getQuantityString(R.plurals.comment_count_format, redditPost.commentCount, redditPost.commentCount)
        // Set the timestamp
        onBindTimestamViewHolder(holder, redditPost)
        // Set the thumbnail
        onBindThumbnailViewHolder(holder, redditPost)
        // Bind click event
        holder.dismissBtn.setOnClickListener {
            callback.onDismissPostBtnClicked(redditPost)
        }
        holder.itemView.setOnClickListener {
            callback.onRedditPostClicked(redditPost)
        }
    }

    private fun onBindThumbnailViewHolder(holder: RedditPostViewHolder, redditPost: RedditPost) {
        GlideApp.with(holder.thumbnailImageView)
            .loadRedditThumbnail(redditPost, R.drawable.drawable_thumbnail_placeholder)
            .into(holder.thumbnailImageView)
    }

    private fun onBindTimestamViewHolder(holder: RedditPostViewHolder, redditPost: RedditPost) {
        val context = holder.itemView.context
        val text = redditPost.createdAt?.let { timeStampDate ->
            val daysDifference = TimeUnit.DAYS.convert(
                calendar.timeInMillis - timeStampDate.time, TimeUnit.MILLISECONDS
            ).toInt()
            when (daysDifference) {
                0 -> {
                    // It's today we should display only hours ago.
                    val hoursAgo = TimeUnit.MILLISECONDS.toHours(
                        calendar.timeInMillis - timeStampDate.time
                    )
                    if (hoursAgo <= 0) {
                        val minutesAgo = TimeUnit.MILLISECONDS.toMinutes(
                            calendar.timeInMillis - timeStampDate.time
                        )
                        if (minutesAgo <= 0) {
                            context.getString(R.string.notification_list_now)
                        } else {
                            context.resources.getQuantityString(R.plurals.notification_list_today_minutes_format,
                                minutesAgo.toInt(), minutesAgo.toString())
                        }
                    } else {
                        context.resources.getQuantityString(R.plurals.notification_list_today_format,
                            hoursAgo.toInt(), hoursAgo.toString())
                    }
                }
                1 -> {
                    // Yesterday
                    val time = timeFormatter.format(timeStampDate).toLowerCase()
                    context.getString(R.string.notification_list_yesterday_format, time)
                }
                in 2..7 -> {
                    // It was during the week just display the day's name
                    val day = dayFormatter.format(timeStampDate)
                    val time = timeFormatter.format(timeStampDate).toLowerCase()
                    context.getString(R.string.notification_list_week_format, day, time)
                }
                in 7..365 -> {
                    // It was before the past week, but within the year, we display the full month
                    // plus date format
                    val date = fullDateFormatter.format(timeStampDate)
                    val time = timeFormatter.format(timeStampDate).toLowerCase()
                    context.getString(R.string.notification_list_week_format, date, time)
                }
                else -> {
                    // If it was more than a year ago, we include the year
                    val date = fullYearFormatter.format(timeStampDate)
                    val time = timeFormatter.format(timeStampDate).toLowerCase()
                    context.getString(R.string.notification_list_week_format, date, time)
                }
            }
        }
        if (text.isBlank()) {
            holder.createdAtTextView.visibility = View.GONE
        } else {
            holder.createdAtTextView.visibility = View.VISIBLE
            holder.createdAtTextView.text = text
        }
    }
}
