package com.kimboo.examples.ui.detail.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kimboo.core.interactors.GetSingleRedditPostInteractor
import com.kimboo.core.interactors.MarkRedditPostAsSeenInteractor
import com.kimboo.core.models.RedditPost
import javax.inject.Inject

class RedditDetailViewModel @Inject constructor(
    private val markRedditPostAsSeenInteractor: MarkRedditPostAsSeenInteractor,
    private val getSingleRedditPostInteractor: GetSingleRedditPostInteractor
) : ViewModel(), MarkRedditPostAsSeenInteractor.Callback, GetSingleRedditPostInteractor.Callback {

    // region Sealed classes declaration
    sealed class State {
        data class Success(
            val redditPost: RedditPost
        ) : State()
        object Error : State()
    }
    // endregion

    // region Variables declaration
    val state = MutableLiveData<State>()
    // endregion

    // region GetSingleRedditPostInteractor.Callback implementation
    override fun onSuccessfullyFetchedRedditPost(redditPost: RedditPost) {
        // Once we successfully load the Reddit post from cache we mark it as seen
        markRedditPostAsSeenInteractor.execute(this, redditPost.id)
        state.value = State.Success(redditPost)
    }

    override fun onErrorFetchingRedditPost() {
        state.value = State.Error
    }
    // endregion

    // region MarkRedditPostAsSeenUseCase.Callback implementation
    override fun onRedditPostSuccessfullyMarkedAsSeen() {
        // NOOP
    }

    override fun onErrorTryingToMarkTheRedditPostAsSeen() {
        // NOOP
    }

    fun fetchRedditPostById(redditPostId: String) {
        getSingleRedditPostInteractor.execute(this, redditPostId)
    }
    // endregion
}