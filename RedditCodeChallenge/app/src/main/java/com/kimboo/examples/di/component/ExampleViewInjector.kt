package com.kimboo.examples.di.component

import com.kimboo.core.di.component.BaseSubComponent
import com.kimboo.examples.di.modules.RedditViewModelModule
import com.kimboo.examples.ui.detail.RedditDetailFragment
import com.kimboo.examples.ui.list.RedditListActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(dependencies = [BaseSubComponent::class],
    modules = [RedditViewModelModule::class]
)
interface ExampleViewInjector {
    fun inject(activity: RedditListActivity)
    fun inject(activity: RedditDetailFragment)
    @Component.Builder
    interface Builder {
        fun baseSubComponent(baseSubComponent: BaseSubComponent): Builder
        fun build(): ExampleViewInjector
    }
}