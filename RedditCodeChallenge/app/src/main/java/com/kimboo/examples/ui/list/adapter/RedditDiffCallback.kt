package com.kimboo.examples.ui.list.adapter

import androidx.recyclerview.widget.DiffUtil
import com.kimboo.core.models.RedditPost

/**
 * Simple standard diff callback that we use to update the adapter in the [RedditListActivity]
 */
class RedditDiffCallback(
    private val oldRedditPosts: List<RedditPost>,
    private val newRedditPosts: List<RedditPost>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldRedditPosts.getOrNull(oldItemPosition)?.id == newRedditPosts.getOrNull(newItemPosition)?.id
    }

    override fun getOldListSize() = oldRedditPosts.size

    override fun getNewListSize() = newRedditPosts.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldRedditPosts.getOrNull(oldItemPosition) == newRedditPosts.getOrNull(newItemPosition)
    }
}