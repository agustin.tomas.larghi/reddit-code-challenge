package com.kimboo.examples.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kimboo.examples.R

class RedditDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.item_detail_container, RedditDetailFragment.newInstance(intent.getStringExtra(ARG_BUNDLE_REDDIT_POST_ID)))
                .commit()
        }
    }

    companion object {
        private const val ARG_BUNDLE_REDDIT_POST_ID = "ARG_BUNDLE_REDDIT_POST_ID"

        fun getStartIntent(context: Context, redditPostId: String): Intent {
            return Intent(context, RedditDetailActivity::class.java).apply {
                putExtra(ARG_BUNDLE_REDDIT_POST_ID, redditPostId)
            }
        }
    }
}