package com.kimboo.examples.di.modules

import androidx.lifecycle.ViewModel
import com.kimboo.core.utils.ViewModelKey
import com.kimboo.examples.ui.detail.viewmodel.RedditDetailViewModel
import com.kimboo.examples.ui.list.viewmodel.RedditListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class RedditViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(RedditListViewModel::class)
    abstract fun bindMainViewModel(
        redditListViewModel: RedditListViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RedditDetailViewModel::class)
    abstract fun bindRedditDetailViewModel(
        redditDetailViewModel: RedditDetailViewModel
    ): ViewModel
}