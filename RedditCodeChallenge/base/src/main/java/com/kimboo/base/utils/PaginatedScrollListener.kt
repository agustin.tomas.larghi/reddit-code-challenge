package com.kimboo.base.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * A very simple pagination scroll listener. It uses a isLoading variable to keep track of whether we are
 * already fetching the next page, and the offset is set to -1, meaning that we will trigger the call to the
 * [onFetchNextPage] when the before last item is completely visible.
 */
abstract class PaginatedScrollListener : RecyclerView.OnScrollListener() {
    var isLoading: Boolean = true
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        if (!isLoading) {
            if (layoutManager.findLastCompletelyVisibleItemPosition() >= layoutManager.itemCount - 1) {
                isLoading = true
                onFetchNextPage()
            }
        }
    }

    /**
     * Function triggered when we are about to reach the end of the list.
     */
    abstract fun onFetchNextPage()
}