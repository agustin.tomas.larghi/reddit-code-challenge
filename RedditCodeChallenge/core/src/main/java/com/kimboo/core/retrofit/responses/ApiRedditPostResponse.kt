package com.kimboo.core.retrofit.responses

import com.google.gson.annotations.SerializedName
import java.util.*

data class ApiRedditPostResponse (
    @SerializedName("name")
    val id : String,
    @SerializedName("author")
    val author : String,
    @SerializedName("thumbnail")
    val thumbnailUrl : String,
    @SerializedName("title")
    val title : String,
    @SerializedName("created_utc")
    val createdUtc : Date,
    @SerializedName("num_comments")
    val numComments : Int
)