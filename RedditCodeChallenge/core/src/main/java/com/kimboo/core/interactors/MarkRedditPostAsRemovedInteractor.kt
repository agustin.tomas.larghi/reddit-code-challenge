package com.kimboo.core.interactors

/**
 * Interactor that marks one single Reddit post as "removed" in the database.
 */
interface MarkRedditPostAsRemovedInteractor {
    interface Callback {
        /**
         * Callback triggered once we successfully mark the reddit post as "removed".
         */
        fun onSuccessfullyRemovedRedditPost()

        /**
         * Callback triggered if something goes wrong while trying to mark the reddit post as "removed".
         */
        fun onErrorTryingToRemoveRedditPost()
    }

    /**
     * The execute function that triggers this Interactor.
     * @param callback The callback taht we use to communicate back to the ViewModel.
     * @param redditPostId The id of the post that we want to mark as "removed".
     */
    fun execute(callback: Callback, redditPostId: String)
}