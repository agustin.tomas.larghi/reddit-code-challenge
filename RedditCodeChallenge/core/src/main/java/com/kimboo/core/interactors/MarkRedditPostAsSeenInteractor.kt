package com.kimboo.core.interactors

/**
 * Interactor that marks one single Reddit post as "seen" on the DB.
 */
interface MarkRedditPostAsSeenInteractor {
    interface Callback {
        fun onRedditPostSuccessfullyMarkedAsSeen()
        fun onErrorTryingToMarkTheRedditPostAsSeen()
    }

    fun execute(callback: Callback, redditPostId: String)
}