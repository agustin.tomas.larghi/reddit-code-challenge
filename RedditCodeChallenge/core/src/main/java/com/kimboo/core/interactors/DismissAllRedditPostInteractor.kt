package com.kimboo.core.interactors

/**
 * An Interactor that marks all the Reddit posts in the DB as "removed",
 */
interface DismissAllRedditPostInteractor {
    interface Callback {
        /**
         * Callback triggered once we successfully marked all reddit posts as "removed".
         */
        fun onSuccessfullyRemovedAllRedditPosts()

        /**
         * Callback triggered if something goes wrong while trying to mark all reddit posts as "removed"
         */
        fun onErrorTryingToRemoveAllRedditPosts()
    }

    /**
     * The execute function to trigger this Interactor.
     * @param callback The callback that we use to communicate with the ViewModel.
     */
    fun execute(callback: Callback)
}