package com.kimboo.core.interactors

import com.kimboo.core.models.RedditPost

/**
 * Returns one single Reddit post from the DB.
 */
interface GetSingleRedditPostInteractor {
    interface Callback {
        /**
         * Callback triggered once we successfully fetch the reddit post from the DB.
         */
        fun onSuccessfullyFetchedRedditPost(redditPost: RedditPost)

        /**
         * Callback triggered if something goes wrong while trying to fetch the Reddit post.
         */
        fun onErrorFetchingRedditPost()
    }

    /**
     * The execute function that triggers this interactor.
     * @param callback The callback that we use to communicate back with the ViewModel.
     * @param redditPostId The id of the post that we want to fetch from the DB.
     */
    fun execute(callback: Callback, redditPostId: String)
}