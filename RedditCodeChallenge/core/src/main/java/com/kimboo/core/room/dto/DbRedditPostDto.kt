package com.kimboo.core.room.dto

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    tableName = "db_reddit_post_repository_dto",
    indices = [Index(
        value = ["id"],
        unique = true
    )]
)
data class DbRedditPostDto(
    @PrimaryKey
    val id: String,
    val userName: String,
    val commentCount: Int,
    val titleText: String,
    val createdAt: Date,
    val hasBeenRead: Boolean?,
    val hasBeenRemoved: Boolean?,
    val thumbnailUrl: String
)