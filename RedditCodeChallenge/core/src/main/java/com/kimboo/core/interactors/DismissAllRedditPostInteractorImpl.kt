package com.kimboo.core.interactors

import com.kimboo.core.repositories.RedditCacheRepository
import com.kimboo.core.repositories.RedditNetworkRepository

class DismissAllRedditPostInteractorImpl(
    private val redditCacheRepository: RedditCacheRepository,
    private val redditNetworkRepository: RedditNetworkRepository
) : DismissAllRedditPostInteractor {
    override fun execute(
        callback: DismissAllRedditPostInteractor.Callback
    ) {
        // First we fetch the last available reddit post
        redditCacheRepository.markAllRedditPostsAsRemoved()
        .subscribe(
            {
                callback.onSuccessfullyRemovedAllRedditPosts()
            },
            {
                callback.onErrorTryingToRemoveAllRedditPosts()
            }
        )
    }
}