package com.kimboo.core.interactors

import com.kimboo.core.repositories.RedditCacheRepository

class MarkRedditPostAsSeenInteractorImpl(
    private val redditCacheRepository: RedditCacheRepository
) : MarkRedditPostAsSeenInteractor {
    override fun execute(
        callback: MarkRedditPostAsSeenInteractor.Callback,
        redditPostId: String
    ) {
        // We get the actual reddit post that we are looking for.
        redditCacheRepository.getRedditPostById(redditPostId).flatMap {
            val redditPost = it.response
            // Update the state to "seen"
            val updatedRedditPost = redditPost.copy(hasBeenRead = true)
            // Update the DB.
            redditCacheRepository.updateRedditPost(updatedRedditPost)
        }
        .subscribe(
            {
                if (it.response) {
                    callback.onRedditPostSuccessfullyMarkedAsSeen()
                } else {
                    callback.onErrorTryingToMarkTheRedditPostAsSeen()
                }
            },
            {
                callback.onErrorTryingToMarkTheRedditPostAsSeen()
            }
        )
    }
}