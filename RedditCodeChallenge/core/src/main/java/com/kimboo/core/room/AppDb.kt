package com.kimboo.core.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kimboo.core.room.converters.RoomConverters
import com.kimboo.core.room.dao.RedditDao
import com.kimboo.core.room.dto.DbRedditPostDto

@Database(
    entities = [DbRedditPostDto::class],
    version = 11,
    exportSchema = false
)
@TypeConverters(RoomConverters::class)
abstract class AppDb : RoomDatabase() {

    companion object {
        fun create(context: Context, useInMemory: Boolean): AppDb {
            val databaseBuilder = if (useInMemory) {
                Room.inMemoryDatabaseBuilder(context, AppDb::class.java)
            } else {
                Room.databaseBuilder(context, AppDb::class.java, "reddit_example.db")
            }
            return databaseBuilder
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    abstract fun redditRepositoryDao(): RedditDao

}
