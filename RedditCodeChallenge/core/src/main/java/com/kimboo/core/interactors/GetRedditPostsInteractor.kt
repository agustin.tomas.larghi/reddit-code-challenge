package com.kimboo.core.interactors

import com.kimboo.core.models.RedditPost

/**
 * An interactor that fetches the RedditPosts. Internally it works via [Flowables] so the callback will
 * be triggered every time there's a change in the database.
 */
interface GetRedditPostsInteractor {
    interface Callback {
        /**
         * Callback triggered when the [Flowable] from the DB reacts against a change.
         */
        fun onRedditPostsSuccessfullyFetched(
            repositories: List<RedditPost>
        )

        /**
         * Callback triggered when something goes wrong.
         */
        fun onErrorFetchingRedditPosts()

        /**
         * Callback triggered once we are done querying the backend.
         */
        fun onDoneLoadingFromApi(it: List<RedditPost>?)
    }

    /**
     * The execute function that triggers the interactor.
     * @param callback The callback that we use to communicate back to the ViewModel.
     * @param fetchNextPage If it's true it will fetch the id from the last Reddit post stored in the
     * DB and ask for the next page. If false it will just query the backend without any pagination information
     * fetching the top tem posts.
     */
    fun execute(callback: Callback, fetchNextPage: Boolean)
}