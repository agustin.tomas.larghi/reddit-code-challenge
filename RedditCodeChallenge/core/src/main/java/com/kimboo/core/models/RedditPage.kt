package com.kimboo.core.models

data class RedditPage(
    val list: List<RedditPost>,
    val after: String,
    val before: String
)