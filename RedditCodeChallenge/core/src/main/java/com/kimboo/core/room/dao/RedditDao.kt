package com.kimboo.core.room.dao

import androidx.room.*
import com.kimboo.core.room.dto.DbRedditPostDto
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface RedditDao {
    @Query("SELECT * FROM db_reddit_post_repository_dto where hasBeenRemoved = :showRemovedPosts")
    fun getAllRedditPosts(
        showRemovedPosts: Boolean = false
    ): Flowable<List<DbRedditPostDto>>

    @Query("SELECT * FROM db_reddit_post_repository_dto where id = :id")
    fun getRedditPostById(id: String): Single<DbRedditPostDto>

    @Query("SELECT * FROM db_reddit_post_repository_dto")
    fun getAllAvailableRedditPosts(): Single<List<DbRedditPostDto>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun storeRedditPosts(
        dbRedditPostDtos: List<DbRedditPostDto>
    ): Single<LongArray>

    @Update
    fun updateRedditPost(dbRedditPostDto: DbRedditPostDto): Single<Int>

    @Query("UPDATE db_reddit_post_repository_dto SET hasBeenRemoved = :markAsRemoved")
    fun markAllRedditPostsAsRemoved(
        markAsRemoved: Boolean = true
    ): Single<Int>
}