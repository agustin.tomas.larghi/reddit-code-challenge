package com.kimboo.core.interactors

import com.kimboo.core.mappers.fromDtoToRedditPostList
import com.kimboo.core.mappers.toDto
import com.kimboo.core.mappers.toRedditPostList
import com.kimboo.core.models.RedditPost
import com.kimboo.core.repositories.RedditCacheRepository
import com.kimboo.core.repositories.RedditNetworkRepository
import com.kimboo.core.utils.DataResponse
import io.reactivex.Observable

class GetRedditPostsInteractorFlowableImpl(
    private val redditNetworkRepository: RedditNetworkRepository,
    private val redditCacheRepository: RedditCacheRepository
) : GetRedditPostsInteractor {

    override fun execute(
        callback: GetRedditPostsInteractor.Callback,
        fetchNextPage: Boolean
    ) {
        // Subscribe to the Flowable query so we can listen for any changes from the DB
        redditCacheRepository.getAllRedditPosts()
            .flatMap {
                if (it.response.isEmpty()) {
                    // If the cache returns empty that probably means that we dismissed all posts. We grab the last
                    // reddit post and query the next page.
                    redditCacheRepository.getLastRedditPost().flatMap {
                        getRedditPostsFromApi(callback, it.response.id)
                    }.flatMap {
                        // Again we ask for all reddit posts
                        redditCacheRepository.getAllRedditPosts()
                    }
                } else {
                    // If the content in the DB isn't empty we just return whatever we got.
                    Observable.just(it)
                }
            }
            // Map the DB rows into model classes
            .map { it.response.fromDtoToRedditPostList() }
            .subscribe(
                {
                    callback.onRedditPostsSuccessfullyFetched(it)
                },
                {
                    callback.onErrorFetchingRedditPosts()
                }
            )

        if (fetchNextPage) {
            // If we are supposed to fetch the next page, query the last reddit post from the DB
            // and query the next page from the backend.
            redditCacheRepository.getLastRedditPost().flatMap {
                getRedditPostsFromApi(callback, it.response.id)
            }
        } else {
            // We are doing PTR or we are loading the reddit posts for the first time.
            // Perform a call to the backend API to get the Reddit posts.
            getRedditPostsFromApi(callback, null)
        }
        .subscribe()
    }

    private fun insertOrUpdateRedditPosts(it: List<RedditPost>): Observable<DataResponse<Boolean>> {
        return redditCacheRepository.storeRedditPosts(it.toDto())
    }

    private fun getRedditPostsFromApi(
        callback: GetRedditPostsInteractor.Callback,
        from: String?
    ): Observable<DataResponse<Boolean>> {
        // Fetches the Reddit posts from the API
        return redditNetworkRepository.fetchRedditPosts(from)
            // Maps the backend responses
            .map { it.response.toRedditPostList() }
            .doOnNext {
                callback.onDoneLoadingFromApi(it)
            }
            // If everything goes okay, it merges the response in the database
            .flatMap {
                insertOrUpdateRedditPosts(it)
            }
            // If something goes wrong, it returns a cached response from the DB
            .onErrorResumeNext { _:Throwable ->
                // Call the callback so we display the message anyways
                callback.onErrorFetchingRedditPosts()
                // Return an empty observable so it doesn't crash
                Observable.just(DataResponse(false, DataResponse.Status.SUCCESS))
            }

    }

}
