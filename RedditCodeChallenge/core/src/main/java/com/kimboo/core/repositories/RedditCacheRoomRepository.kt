package com.kimboo.core.repositories

import com.kimboo.core.room.dao.RedditDao
import com.kimboo.core.room.dto.DbRedditPostDto
import com.kimboo.core.utils.DataResponse
import io.reactivex.Observable
import io.reactivex.Scheduler

class RedditCacheRoomRepository (
    private val uiScheduler: Scheduler,
    private val backgroundScheduler: Scheduler,
    private val redditDao: RedditDao
) : RedditCacheRepository {
    override fun updateRedditPost(
        redditPostDto: DbRedditPostDto
    ): Observable<DataResponse<Boolean>> {
        return redditDao.updateRedditPost(redditPostDto)
            .toObservable()
            .map { DataResponse(it > 0, DataResponse.Status.CACHED_RESPONSE) }
            .observeOn(uiScheduler)
            .subscribeOn(backgroundScheduler)
    }

    override fun getRedditPostById(redditPostId: String): Observable<DataResponse<DbRedditPostDto>> {
        return redditDao.getRedditPostById(redditPostId)
            .toObservable()
            .map { DataResponse(it, DataResponse.Status.CACHED_RESPONSE) }
            .observeOn(uiScheduler)
            .subscribeOn(backgroundScheduler)
    }

    override fun getLastRedditPost(): Observable<DataResponse<DbRedditPostDto>> {
        return redditDao.getAllAvailableRedditPosts()
            .toObservable()
            .flatMap { Observable.just(it.last()) }
            .map { DataResponse(it, DataResponse.Status.CACHED_RESPONSE) }
            .observeOn(uiScheduler)
            .subscribeOn(backgroundScheduler)
    }

    override fun getAllRedditPosts(): Observable<DataResponse<List<DbRedditPostDto>>> {
        return redditDao.getAllRedditPosts()
            .toObservable()
            .map { DataResponse(it, DataResponse.Status.CACHED_RESPONSE) }
            .observeOn(uiScheduler)
            .subscribeOn(backgroundScheduler)
    }

    override fun storeRedditPosts(
        redditPostDtos: List<DbRedditPostDto>
    ): Observable<DataResponse<Boolean>> {
        return redditDao.storeRedditPosts(redditPostDtos)
            .map { DataResponse(it.isNotEmpty(), DataResponse.Status.CACHED_RESPONSE) }
            .toObservable()
            .observeOn(uiScheduler)
            .subscribeOn(backgroundScheduler)
    }

    override fun markAllRedditPostsAsRemoved(): Observable<DataResponse<Boolean>> {
        return redditDao.markAllRedditPostsAsRemoved()
            .map { DataResponse(it > 0, DataResponse.Status.CACHED_RESPONSE) }
            .toObservable()
            .observeOn(uiScheduler)
            .subscribeOn(backgroundScheduler)
    }
}