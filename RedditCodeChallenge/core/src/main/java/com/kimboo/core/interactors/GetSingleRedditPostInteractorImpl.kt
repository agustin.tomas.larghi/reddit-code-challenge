package com.kimboo.core.interactors

import com.kimboo.core.mappers.toRedditPost
import com.kimboo.core.repositories.RedditCacheRepository

class GetSingleRedditPostInteractorImpl(
    private val redditCacheRepository: RedditCacheRepository
) : GetSingleRedditPostInteractor {
    override fun execute(
        callback: GetSingleRedditPostInteractor.Callback,
        redditPostId: String
    ) {
        redditCacheRepository.getRedditPostById(redditPostId)
            .map { it.response.toRedditPost() }
            .subscribe(
                {
                    callback.onSuccessfullyFetchedRedditPost(it)
                },
                {
                    callback.onErrorFetchingRedditPost()
                }
            )
    }
}