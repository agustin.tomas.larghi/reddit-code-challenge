package com.kimboo.core.interactors

import com.kimboo.core.repositories.RedditCacheRepository

class MarkRedditPostAsRemovedInteractorImpl(
    private val redditCacheRepository: RedditCacheRepository
) : MarkRedditPostAsRemovedInteractor {
    override fun execute(
        callback: MarkRedditPostAsRemovedInteractor.Callback,
        redditPostId: String
    ) {
        redditCacheRepository.getRedditPostById(redditPostId).flatMap {
            val redditPost = it.response
            val updatedRedditPost = redditPost.copy(hasBeenRemoved = true)
            redditCacheRepository.updateRedditPost(updatedRedditPost)
        }
        .subscribe(
            {
                if (it.response) {
                    callback.onSuccessfullyRemovedRedditPost()
                } else {
                    callback.onErrorTryingToRemoveRedditPost()
                }
            },
            {
                callback.onErrorTryingToRemoveRedditPost()
            }
        )
    }
}