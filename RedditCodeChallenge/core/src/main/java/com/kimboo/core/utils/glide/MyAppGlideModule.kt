package com.kimboo.core.utils.glide

import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.signature.ObjectKey
import com.kimboo.core.models.RedditPost

@GlideModule
class MyAppGlideModule : AppGlideModule()

fun GlideRequests.loadRedditThumbnail(
    redditPost: RedditPost,
    @DrawableRes placeholder: Int
): GlideRequest<Drawable> {
    return load(redditPost.thumbnailUrl).apply {
        placeholder(placeholder)
        transform(RoundedCorners(16))
        transform(CenterCrop())
        transition(DrawableTransitionOptions.withCrossFade())
        signature(ObjectKey(redditPost.thumbnailUrl))
    }
}