package com.kimboo.core.utils.gson

import java.lang.reflect.Type
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.util.*

/**
 * [Date] deserializer that turns a UTC timestamp into an actual [Date] based on the user's timezone.
 */
class DateDeserializer : JsonDeserializer<Date> {
    override fun deserialize(
        element: JsonElement,
        arg1: Type,
        arg2: JsonDeserializationContext
    ): Date? {
        val utcDateLong = element.asLong
        return Calendar.getInstance().apply {
            timeZone = TimeZone.getTimeZone("UTC")
            timeInMillis = utcDateLong * 1000
        }.time
    }
}