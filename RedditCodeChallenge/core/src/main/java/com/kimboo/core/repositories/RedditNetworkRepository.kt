package com.kimboo.core.repositories

import com.kimboo.core.retrofit.responses.ApiRedditDataResponse
import com.kimboo.core.retrofit.responses.ApiRedditPaginatedPostResponse
import com.kimboo.core.retrofit.responses.ApiRedditPostResponse
import com.kimboo.core.utils.DataResponse
import io.reactivex.Observable

interface RedditNetworkRepository {
    fun fetchRedditPosts(from: String?) : Observable<DataResponse<ApiRedditDataResponse<ApiRedditPaginatedPostResponse>>>
}