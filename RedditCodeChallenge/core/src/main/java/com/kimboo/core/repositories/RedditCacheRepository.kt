package com.kimboo.core.repositories

import com.kimboo.core.room.dto.DbRedditPostDto
import com.kimboo.core.utils.DataResponse
import io.reactivex.Observable

interface RedditCacheRepository {
    fun markAllRedditPostsAsRemoved(): Observable<DataResponse<Boolean>>

    /**
     * Updates the DB against the [redditPostDto] parameter.
     * @param redditPostDto The DB entity that we want to update
     */
    fun updateRedditPost(
        redditPostDto: DbRedditPostDto
    ): Observable<DataResponse<Boolean>>

    /**
     * Gets all the DB entities that we have on the reddit dto table. It uses [Flowable]
     * internally.
     */
    fun getAllRedditPosts(): Observable<DataResponse<List<DbRedditPostDto>>>

    /**
     * Gets the DB entity that matches [redditPostId]. It uses [Flowable] internally
     */
    fun getRedditPostById(
        redditPostId: String
    ): Observable<DataResponse<DbRedditPostDto>>

    fun getLastRedditPost(): Observable<DataResponse<DbRedditPostDto>>

    /**
     * Inserts the collection of [redditPostDtos] into the DB.
     */
    fun storeRedditPosts(
        redditPostDtos: List<DbRedditPostDto>
    ): Observable<DataResponse<Boolean>>
}