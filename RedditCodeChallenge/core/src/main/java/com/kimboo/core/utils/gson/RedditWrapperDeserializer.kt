package com.kimboo.core.utils.gson

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * A custom deserializers that uses the generic arg TYPE to deserialize on the fly the json responses from
 * the API.
 */
class RedditWrapperDeserializer<TYPE>(
    private val castClazz: Class<TYPE>,
    private val isList: Boolean
) : JsonDeserializer<TYPE> {

    val gson = Gson()

    override fun deserialize(
        element: JsonElement,
        arg1: Type,
        arg2: JsonDeserializationContext
    ): TYPE? {
        val jsonObject = element.asJsonObject

        return if (isList) {
            val type = TypeToken.getParameterized(List::class.java, castClazz).type
            gson.fromJson(jsonObject.get("data"), type)
        } else {
            gson.fromJson(jsonObject.get("data"), castClazz)
        }
    }
}