package com.kimboo.core.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class RedditPost(
    val id: String,
    val userName: String,
    val commentCount: Int,
    val titleText: String,
    val createdAt: Date,
    val hasBeenRead: Boolean,
    val thumbnailUrl: String
) : Parcelable