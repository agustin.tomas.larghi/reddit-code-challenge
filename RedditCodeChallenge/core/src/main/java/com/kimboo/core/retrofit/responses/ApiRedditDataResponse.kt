package com.kimboo.core.retrofit.responses

import com.google.gson.annotations.SerializedName

data class ApiRedditDataResponse<T> (
    @SerializedName("data")
    val data : T
)