package com.kimboo.core.di.modules

import com.kimboo.core.repositories.RedditCacheRepository
import com.kimboo.core.repositories.RedditCacheRoomRepository
import com.kimboo.core.repositories.RedditNetworkRepositoryImpl
import com.kimboo.core.repositories.RedditNetworkRepository
import com.kimboo.core.retrofit.api.RedditApi
import com.kimboo.core.room.dao.RedditDao
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class RepositoryModule {
    @Provides
    fun providesRedditNetworkRepository(
        @Named("uiScheduler") uiScheduler: Scheduler,
        @Named("backgroundScheduler") backgroundScheduler: Scheduler,
        redditApi: RedditApi
    ) : RedditNetworkRepository {
        return RedditNetworkRepositoryImpl(
            uiScheduler = uiScheduler,
            backgroundScheduler = backgroundScheduler,
            retrofitApi = redditApi
        )
    }

    @Provides
    fun providesRedditCacheRepository(
        @Named("uiScheduler") uiScheduler: Scheduler,
        @Named("backgroundScheduler") backgroundScheduler: Scheduler,
        redditDao: RedditDao
    ) : RedditCacheRepository {
        return RedditCacheRoomRepository(
            uiScheduler = uiScheduler,
            backgroundScheduler = backgroundScheduler,
            redditDao = redditDao
        )
    }
}