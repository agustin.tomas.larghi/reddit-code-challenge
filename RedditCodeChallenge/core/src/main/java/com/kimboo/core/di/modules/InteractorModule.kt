package com.kimboo.core.di.modules

import com.kimboo.core.interactors.*
import com.kimboo.core.repositories.RedditCacheRepository
import com.kimboo.core.repositories.RedditNetworkRepository
import dagger.Module
import dagger.Provides

@Module
class InteractorModule {
    @Provides
    fun provideGetRedditPostsRepository(
        redditNetworkRepository: RedditNetworkRepository,
        redditCacheRepository: RedditCacheRepository
    ) : GetRedditPostsInteractor {
        return GetRedditPostsInteractorFlowableImpl(
            redditNetworkRepository = redditNetworkRepository,
            redditCacheRepository = redditCacheRepository
        )
    }

    @Provides
    fun providesMarkRedditPostAsRemovedInteractor(
        redditCacheRepository: RedditCacheRepository
    ) : MarkRedditPostAsRemovedInteractor {
        return MarkRedditPostAsRemovedInteractorImpl(
            redditCacheRepository = redditCacheRepository
        )
    }

    @Provides
    fun provideMarkRedditPostAsSeenUseCase(
        redditCacheRepository: RedditCacheRepository
    ) : MarkRedditPostAsSeenInteractor {
        return MarkRedditPostAsSeenInteractorImpl(
            redditCacheRepository = redditCacheRepository
        )
    }

    @Provides
    fun providesGetSingleRedditPostInteractor(
        redditCacheRepository: RedditCacheRepository
    ) : GetSingleRedditPostInteractor {
        return GetSingleRedditPostInteractorImpl(
            redditCacheRepository = redditCacheRepository
        )
    }

    @Provides
    fun providesMarkAllRedditPostAsRemovedInteractor(
        redditCacheRepository: RedditCacheRepository,
        redditNetworkRepository: RedditNetworkRepository
    ) : DismissAllRedditPostInteractor {
        return DismissAllRedditPostInteractorImpl(
            redditCacheRepository = redditCacheRepository,
            redditNetworkRepository = redditNetworkRepository
        )
    }
}