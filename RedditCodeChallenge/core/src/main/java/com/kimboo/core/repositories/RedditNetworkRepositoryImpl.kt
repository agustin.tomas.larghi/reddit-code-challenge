package com.kimboo.core.repositories

import com.kimboo.core.retrofit.api.RedditApi
import com.kimboo.core.retrofit.responses.ApiRedditDataResponse
import com.kimboo.core.retrofit.responses.ApiRedditPaginatedPostResponse
import com.kimboo.core.retrofit.responses.ApiRedditPostResponse
import com.kimboo.core.utils.DataResponse
import com.kimboo.core.utils.toDataResponse
import io.reactivex.Observable
import io.reactivex.Scheduler

class RedditNetworkRepositoryImpl (
    private val retrofitApi: RedditApi,
    private val uiScheduler: Scheduler,
    private val backgroundScheduler: Scheduler
) : RedditNetworkRepository {
    override fun fetchRedditPosts(from: String?): Observable<DataResponse<ApiRedditDataResponse<ApiRedditPaginatedPostResponse>>> {
        return retrofitApi.fetchRedditPosts(
            after = from
        )
        .toDataResponse()
        .observeOn(uiScheduler)
        .subscribeOn(backgroundScheduler)
    }
}
