package com.kimboo.core.mappers

import com.kimboo.core.models.RedditPost
import com.kimboo.core.retrofit.responses.ApiRedditDataResponse
import com.kimboo.core.retrofit.responses.ApiRedditPaginatedPostResponse
import com.kimboo.core.retrofit.responses.ApiRedditPostResponse
import com.kimboo.core.room.dto.DbRedditPostDto

/**
 * Classes with extended functions to turn backend responses into model classes
 */

fun ApiRedditDataResponse<ApiRedditPaginatedPostResponse>.toRedditPostList(): List<RedditPost> {
    return data.children.map {
        it.data.toRedditPost()
    }
}

private fun ApiRedditPostResponse.toRedditPost(): RedditPost {
    return RedditPost(
        id = id,
        commentCount = numComments,
        createdAt = createdUtc,
        hasBeenRead = false,
        titleText = title,
        userName =  author,
        thumbnailUrl = thumbnailUrl
    )
}

fun List<RedditPost>.toDto() : List<DbRedditPostDto> {
    return map {
        DbRedditPostDto(
            id = it.id,
            userName = it.userName,
            titleText = it.titleText,
            hasBeenRead = it.hasBeenRead,
            createdAt = it.createdAt,
            hasBeenRemoved = false,
            commentCount = it.commentCount,
            thumbnailUrl = it.thumbnailUrl
        )
    }
}

fun List<DbRedditPostDto>.fromDtoToRedditPostList() : List<RedditPost> {
    return map {
        it.toRedditPost()
    }
}

fun DbRedditPostDto.toRedditPost(): RedditPost {
    return RedditPost(
        id = id,
        userName = userName,
        titleText = titleText,
        hasBeenRead = hasBeenRead ?: false,
        createdAt = createdAt,
        commentCount = commentCount,
        thumbnailUrl = thumbnailUrl
    )
}
