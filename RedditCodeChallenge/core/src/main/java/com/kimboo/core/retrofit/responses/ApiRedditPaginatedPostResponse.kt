package com.kimboo.core.retrofit.responses

import com.google.gson.annotations.SerializedName

data class ApiRedditPaginatedPostResponse (
    @SerializedName("children")
    val children: List<ApiRedditDataResponse<ApiRedditPostResponse>>,
    @SerializedName("after")
    val after: String?,
    @SerializedName("before")
    val before: String?
)