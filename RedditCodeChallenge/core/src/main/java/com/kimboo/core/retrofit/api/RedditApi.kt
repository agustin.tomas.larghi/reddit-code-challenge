package com.kimboo.core.retrofit.api

import com.kimboo.core.retrofit.responses.ApiRedditDataResponse
import com.kimboo.core.retrofit.responses.ApiRedditPaginatedPostResponse
import com.kimboo.core.retrofit.responses.ApiRedditPostResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit's interface API.
 */
interface RedditApi {
    @GET("/r/{subreddit}/top.json")
    fun fetchRedditPosts(
        @Path("subreddit") subreddit: String = "all",
        @Query("limit") limit: Int = 10,
        @Query("after") after: String? = null
    ): Single<Response<ApiRedditDataResponse<ApiRedditPaginatedPostResponse>>>
}